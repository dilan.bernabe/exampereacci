#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="front"

# Building React output
yarn install
yarn run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/ ubuntu@${DEPLOY_SERVER}:/${SERVER_FOLDER}/
echo "Finished copying the build files"

echo "Run app"
ssh $ubuntu@$${DEPLOY_SERVER} "./run.sh"
echo "Intiza running in port 5000"